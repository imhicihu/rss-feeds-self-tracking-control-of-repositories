![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/3278295154-status_archived.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-closed](https://bitbucket.org/repo/ekyaeEE/images/1555006384-issues_closed.png)

# Rationale #

This repo's purpose is a way to monitor all the RSS feeds of all of our repositories. A kind of `Hub of feeds` with just one  _raison d'�tre_: visualize all data graphically. Some tools in mind: [D3.js](https://d3js.org/); [HighCharts](https://www.highcharts.com/demo) plus some more.

![rss.png](https://bitbucket.org/repo/AgG5e6d/images/2187833784-rss.png)

Actually, Bitbucket disolved rss feeds. This is the [official response](https://community.atlassian.com/t5/Bitbucket-questions/Where-to-find-the-url-for-an-RSS-feed-of-commits/qaq-p/832308)
Our Feedly account got this messages:
![feedly-response.jpeg](https://bitbucket.org/repo/AgG5e6d/images/775919516-937455301-feeder.jpg)
So, this repo goes *deprecated*


### What is this repository for? ###

* Quick summary
	- a kind of  graphical tapestry  based on all actions done, along the time. Essentially, it's a way to get familiarize with most data visualization tools for future  projects
	
	![feed.png](https://bitbucket.org/repo/aBjx4q/images/1626076599-feed.png)
	
* Version
	- 1.1

### How do I get set up? ###

* Summary of set up
	- a [Bitbucket](http://bitbucket.org/) or [Github](http://github.com/) account
	- some [repositories](https://bitbucket.org/imhicihu/) created
	- lots of issues/branches/commits done
	- some expertise on javascript to handle the configuration and setup of `D3.js` on the webpage (`.html`)
	- [Github Pages](https://pages.github.com/) to show graphically the results
	- a [Feedly](https://feedly.com/) account (free tier is `OK`)
	- an account on [Feedly](http://feedly.com/) to verify the online status and their correct _timeline_
	- knowledge of Javascript language (`.js`)
* Configuration
	- ~~In process~~
* Dependencies
	- an internet connection, a computer, a rss viewer or an internet browser. Go to our Github Page (in process). And browse the variables offered.
* Deployment instructions
	- ~~In process~~

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/rss-feeds-self-tracking-control-of-repositories/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
	- Contact: `imhicihu` at `gmail` dot `com`
* Other community or team contact
	- Contact our [Board](https://bitbucket.org/imhicihu/rss-feeds-self-tracking-control-of-repositories/addon/trello/trello-board). (You need a [Trello](https://trello.com/) account)

### Code of Conduct ###

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/rss-feeds-self-tracking-control-of-repositories/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.